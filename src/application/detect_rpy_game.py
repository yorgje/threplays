# detect_rpy_game.py
# returns the game name of the replay

import sys
from StringIO import StringIO


# maps the ids in the replay files to their acronym
name_map = {
    "T6RP": "EoSD",
    "T7RP": "PCB",
    "T8RP": "IN",
    "T9RP": "PoFV",
    "T95R": "StB",
    "T10R": "MoF",
    "T11R": "SA",
    "T12R": "UFO",
    "T125": "DS",
    "128R": "GFW",
    "T13R": "TD",
    "T14R": "DDC",
    "T143": "ISC",
    "T15R": "LoLK"
}


# input - file name or path
# returns - string representing the game this replay is for
#         - "err" is error encountered (bad file)
def getRpyGame(replay_text):
    game_name = ""
    
    rfile = StringIO(replay_text)
    try:
        game_name = rfile.read(4).upper()   # first 4 bytes
        rfile.close()
    except:
        rfile.close()
        return "err"

    # th13 and th14 have T13R as the id in the .rpy file
    if(game_name == "T13R"):
        rfile = open(fname, 'rb')
        repdata = rfile.read()
        rfile.close()

        # it's 2hu 13
        if("938C95FB905F97EC955F".decode('hex') in repdata):
            game_name = "T13R"
        # it's 2hu 14
        else:
            game_name = "T14R"

    if(game_name not in name_map):
        return "err"
    else:
        return name_map[game_name]
# END getRpyGame
