"""
forms.py

Web forms based on Flask-WTForms

See: http://flask.pocoo.org/docs/patterns/wtforms/
     http://wtforms.simplecodes.com/

"""

from flaskext import wtf
from flaskext.wtf import validators
from wtforms.ext.appengine.ndb import model_form

from models import Replay

class ReplayForm(wtf.Form):
    player = wtf.TextField('Player Name')
    description = wtf.TextAreaField('Description', validators=[validators.Length(max=500)])
    runtype = wtf.SelectField('Runtype', choices=[('Full', 'Full Run')
                                                 , ('Stage', 'Stage Practice')
                                                 , ('Spell', 'Spell Practice')
                                                 ])
    tas = wtf.BooleanField('TAS')
    nm = wtf.BooleanField('NM')
    nb = wtf.BooleanField('NB')
    nf = wtf.BooleanField('NF')
    nv = wtf.BooleanField('NV')
    pacifist = wtf.BooleanField('Pacifist')
    other_restriction = wtf.BooleanField('Other Restriction')

class ReplayDeleteForm(wtf.Form):
    password = wtf.PasswordField('Password', validators=[validators.Required()])

class ReplayUploadForm(wtf.Form):
    replay_file = wtf.FileField('File', validators=[validators.Required()])

class ReplayUploadUserInputForm(ReplayForm):
    replay_file = wtf.HiddenField(validators=[validators.Required()])
    game = wtf.HiddenField(validators=[validators.Required()])
    password = wtf.PasswordField('Password')

class ReplayUploadUserInputFormPCB(ReplayUploadUserInputForm):
    year = wtf.DateField('Year Played', format='%Y', validators=[validators.Required()])
    stage = wtf.SelectField("Last Stage",validators=[validators.Required()], choices=[
            ('All Clear', 'All Clear')
            , ('1', 'Stage 1')
            , ('2', 'Stage 2')
            , ('3', 'Stage 3')
            , ('4', 'Stage 4')
            , ('5', 'Stage 5')
            , ('6', 'Stage 6 (non-clear)')
            , ('Extra', 'Extra (non-clear)')
            , ('Phantasm', 'Phantasm (non-clear)')
            ])

class ReplayUploadUserInputFormEoSD(ReplayUploadUserInputForm):
    stage = wtf.SelectField("Last Stage",validators=[validators.Required()], choices=[
            ('All Clear', 'All Clear')
            , ('1', 'Stage 1')
            , ('2', 'Stage 2')
            , ('3', 'Stage 3')
            , ('4', 'Stage 4')
            , ('5', 'Stage 5')
            , ('6', 'Stage 6 (non-clear)')
            , ('Extra', 'Extra (non-clear)')
            ])

class ReplaySearchForm(ReplayForm):
    game = wtf.SelectField('Game', choices=[ ('', '')
                                           , ('PCB', 'PCB')
                                           , ('IN', 'IN')
                                           , ('DDC', 'DDC')
                                           , ('ISC', 'ISC')
                                           , ('LoLK', 'LoLK')
                                           ])
    runtype = wtf.SelectField('Runtype', choices=[ ('', '')
                                                 , ('Full', 'Full Run')
                                                 , ('Stage', 'Stage Practice')
                                                 , ('Spell', 'Spell Practice')
                                                 ])
    char = wtf.TextField('Character')
    rank = wtf.SelectField('Difficulty', choices=[('', '')
                                                 , ('Easy', 'Easy')
                                                 , ('Normal', 'Normal')
                                                 , ('Hard', 'Hard')
                                                 , ('Lunatic', 'Lunatic')
                                                 , ('Extra', 'Extra')
                                                 , ('Phantasm', 'Phantasm')
                                                 ])


