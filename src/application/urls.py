"""
urls.py

URL dispatch route mappings and error handlers

"""
from flask import render_template
from application import app
from application import views


## URL dispatch rules
# App Engine warm up handler
# See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'warmup', view_func=views.warmup)

# Home page
app.add_url_rule('/', 'home', view_func=views.home)

@app.route('/about')
def about():
  return render_template('about.html')
@app.route('/todo')
def todo():
  return render_template('todo.html')
@app.route('/contact')
def contact():
  return render_template('contact.html')

# Serve replays
app.add_url_rule('/replay_files/<int:replay_id>', 'view_replay', view_func=views.view_one_replay)
app.add_url_rule('/replay_files/<int:replay_id>/<filename>', 'serve_replay', view_func=views.serve_replay)

# Replay list page
app.add_url_rule('/replays', 'list_replays', view_func=views.list_replays, methods=['GET'])
app.add_url_rule('/replays/<int:page>', 'list_replays', view_func=views.list_replays, methods=['GET'])
app.add_url_rule('/replays/<int:page>/<sortfield>', 'list_replays', view_func=views.list_replays, methods=['GET'])
app.add_url_rule('/replays/<int:page>/<sortfield>/<sortorder>', 'list_replays', view_func=views.list_replays, methods=['GET'])
app.add_url_rule('/replays/<int:page>/<sortfield>/<sortorder>/<replay_filter>', 'list_replays', view_func=views.list_replays, methods=['GET'])

# Replay search page
app.add_url_rule('/search', 'search', view_func=views.search, methods=['GET', 'POST'])

app.add_url_rule('/upload', 'upload_replay', view_func=views.upload_replay, methods=['POST'])
app.add_url_rule('/upload_static', 'upload_replay_static', view_func=views.upload_replay, methods=['GET', 'POST'])
app.add_url_rule('/upload_user_input', 'upload_user_input', view_func=views.upload_replay_with_user_input, methods=['GET', 'POST'])

# Examples list page (cached)
#app.add_url_rule('/examples/cached', 'cached_examples', view_func=views.cached_examples, methods=['GET'])

# Contrived admin-only view example
#app.add_url_rule('/admin_only', 'admin_only', view_func=views.admin_only)

# Edit a replay
app.add_url_rule('/modify_replay/<int:replay_id>/edit', 'edit_replay', view_func=views.edit_replay, methods=['GET', 'POST'])

# Delete a replay
app.add_url_rule('/modify_replay/<int:replay_id>/delete', 'delete_replay', view_func=views.delete_replay, methods=['GET', 'POST'])

# Update schema
app.add_url_rule('/update_schema', 'update_schema', view_func=views.update_schema, methods=['GET'])


## Error handlers
# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

