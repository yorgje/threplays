"""
models.py

App Engine datastore models

"""


from google.appengine.ext import ndb

class Counter(ndb.Model):
  '''Counts how many replays we have'''
  count = ndb.IntegerProperty(required=True)
  game = ndb.StringProperty(required=True)

class Replay(ndb.Model):
    """
        Replay Model.
    """
    def to_dict(self):
        result = super(Replay,self).to_dict()
        try:
            result['key'] = self.key.id()
        except:
            pass
        return result
    game = ndb.StringProperty(required=True)
    replay_file = ndb.BlobProperty(required=True, indexed=False)
    filename = ndb.StringProperty(required=True)
    player = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty(auto_now_add=True)
    playdate = ndb.DateTimeProperty()
    version = ndb.StringProperty(indexed=False)
    char = ndb.StringProperty()
    #rank = ndb.StringProperty(choices=['Easy', 'Normal', 'Hard', 'Lunatic', 'Extra'])
    rank = ndb.StringProperty()
    stage = ndb.StringProperty()
    score = ndb.StringProperty()
    slow = ndb.StringProperty(indexed=False)
    #runtype = ndb.StringProperty(choices=['Full', 'Stage', 'Spell'])
    runtype = ndb.StringProperty()
    tas = ndb.BooleanProperty()
    nm = ndb.BooleanProperty()
    nb = ndb.BooleanProperty()
    nf = ndb.BooleanProperty()
    nv = ndb.BooleanProperty()
    pacifist = ndb.BooleanProperty()
    other_restriction = ndb.BooleanProperty()
    cheese = ndb.BooleanProperty()
    password = ndb.StringProperty(indexed=False)
