import hashlib
from datetime import datetime
from models import Replay, Counter
import replay_data_grabbers as RDG
import utilities as U
from forms import *
import pickle

fn2ndb = {
        'th6': 'EoSD'
        , 'th7': 'PCB'
        , 'th8':'IN'
        , 'th14': 'DDC'
        , 'th143': 'ISC'
        , 'th15': 'LoLK'
        }
ndb2fn = {v:k for k,v in fn2ndb.items()}

def generate_and_fill_edit_form(replay, game):
    form = generate_edit_form(game)
    form = fill_form(form, replay)
    if game=="PCB":
        form['year'].data = datetime.strptime(str(replay.playdate.year), '%Y')
    return form

def generate_and_fill_user_input_form(replay, game):
    form = generate_user_input_form(game)
    form = fill_form(form, replay)
    form.replay_file.data = pickle.dumps(form.replay_file.data)
    s = form.replay_file.data
    return form

def fill_form(form, replay):
    formdata = form.data
    replay_d = replay.to_dict()
    for field in formdata:
        if field in replay_d:
            if not field=="password":
                form[field].data = replay_d[field]
    return form

def generate_edit_form(game):
    form = generate_user_input_form(game)
    del form.game
    del form.replay_file
    return form

def generate_user_input_form(game):
    form = ReplayUploadUserInputForm()
    if game == 'EoSD':
        form = ReplayUploadUserInputFormEoSD()
    elif game == 'PCB':
        form = ReplayUploadUserInputFormPCB()
    elif game == 'ISC':
        del form.runtype
    return form


def num_to_ext(num):
    num = num % 14776336 # wow imagine having this many replays...
    digits=[]
    for i in reversed(range(4)):
        d = num/62**i
        digits.append(d)
        num -= d * 62**i
    transdigits=[]
    for digit in digits:
        if 0 <= digit and digit <=9:
            transdigits.append(chr(ord('0')+digit))
        elif 10 <= digit and digit <= 35:
            transdigits.append(chr(ord('a')+(digit-10)))
        elif 36 <= digit and digit <= 61:
            transdigits.append(chr(ord('A')+(digit-36)))
    return str("".join(transdigits))

def test_nte():
  for i in range(1000000):
    print num_to_ext(i)

def secure_hash(s):
    return hashlib.sha256(s).hexdigest()

def determine_IN_mistakes(replay_data, field):
    if not replay_data['stage'][:3] == 'No.':
        if not replay_data[field]:
            return not replay_data[field]
    return False

def determine_IN_runtype(form_val, replay_data):
    if replay_data['stage'][:3] == 'No.':
        return 'Spell'
    else:
        return form_val

def get_game_from_fn(fn):
    ext = fn.split('.')[-1].strip()
    assert ext == 'rpy'
    fngame = fn.split('_')[0]
    return fn2ndb[fngame]

def build_PCB_date(year, monthday):
    month,day = map(int, monthday.split('/'))
    return datetime(year, month,day)

def add_replay(form):
    game = form.game.data
    counter = Counter.get_by_id(game)
    if not counter:
        count = Counter(count=0,game=game, id=game)
        count.put()
        num=0
    else:
        counter.count+=1
        counter.put()
        num = counter.count
    replay_text = str(pickle.loads(form.replay_file.data.replace('\r','')))
    replay_dict = build_replay_from_file(replay_text,game).to_dict()
    properties = Replay._properties.keys()
    formdata = form.data
    for field in formdata:
        if field in properties:
            replay_dict[field] = formdata[field]

    if game=='PCB':
        monthday = RDG.get_replay_data(replay_text,game)['date']
        replay_dict['playdate'] = build_PCB_date(form.data['year'].year, monthday)

    if not replay_dict['player']:
        replay_dict['player'] = 'Anonymous'
    replay_dict['filename'] = '%s_ud%s.rpy' % (ndb2fn[game], num_to_ext(num))
    replay_dict['password'] = secure_hash(replay_dict['password'])
    replay_dict['replay_file'] = replay_text
    replay_dict['timestamp'] = datetime.now() # WE SHOULDNT NEED TO DO THIS

    replay = Replay()
    replay.populate(**replay_dict)
    return replay

def build_replay_from_file(replay_text, game):
    replay_data = RDG.get_replay_data(replay_text, game)
    player = replay_data['name']
    if not player.strip():
        player = 'Anonymous'
    elif game=='EoSD':
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , version = replay_data['version']
            , char = replay_data['char']
            , rank = replay_data['rank'].title()
            , score = str(replay_data['score'])
            , slow = replay_data['slow'][:5]
            , playdate = datetime.strptime(replay_data['date'], '%m/%d/%y')
        )
    elif game=='PCB':
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , version = replay_data['version']
            , char = replay_data['char']
            , rank = replay_data['rank'].title()
            , score = str(replay_data['score'])
            , slow = replay_data['slow'][:5]
        )
    elif game=="IN":
        nm = determine_IN_mistakes(replay_data, 'misses')
        nb = determine_IN_mistakes(replay_data, 'bombs')
        nf = (replay_data['human'].split('.')[0] == '100')
        runtype = determine_IN_runtype('Full', replay_data)
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , playdate = replay_data['date']
            , version = replay_data['version']
            , char = replay_data['char']
            , rank = replay_data['rank']
            , stage = replay_data['stage']
            , score = replay_data['score']
            , slow = replay_data['slow']
            , runtype = runtype
            , nm = nm
            , nb = nb
            , nf = nf
        )
    elif game=="DDC":
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , playdate = replay_data['date']
            , version = replay_data['version']
            , char = replay_data['char']
            , rank = replay_data['rank']
            , stage = replay_data['stage']
            , score = replay_data['score']
            , slow = replay_data['slow']
        )

    elif game == "ISC":
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , playdate = replay_data['date']
            , version = replay_data['version']
            , char = "Seija"
            , rank = "--"
            , stage = replay_data['stage']
            , score = replay_data['score']
            , slow = replay_data['slow']
            , runtype = "Spell"
        )
    elif game=="LoLK":
        replay = Replay(
            replay_file=replay_text
            , game = game
            , player=player
            , playdate = replay_data['date']
            , version = replay_data['version']
            , char = replay_data['char']
            , rank = replay_data['rank']
            , stage = replay_data['stage']
            , score = replay_data['score']
            , slow = replay_data['slow']
        )
    return replay
