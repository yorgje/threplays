"""
views.py

URL route handlers
"""
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

from flask import request, render_template, flash, url_for, redirect, make_response, session, Markup

from flask_cache import Cache

from application import app
from decorators import login_required, admin_required
from forms import *
from models import Replay, Counter

from utilities import *
from new_replay_handler import *
from sort_and_filter_helpers import *
from detect_rpy_game import *
from threpinfo import *
import logging
import datetime as dt

# Flask-Cache (configured to use App Engine Memcache API)
cache = Cache(app)

def home():
    return redirect(url_for('list_replays'))

def view_one_replay(replay_id):
    ''' Displays info about a replay '''
    replay = Replay.get_by_id(replay_id)
    replay.score = commas(replay.score)
    replay_details = get_replay_details(replay)
    return render_template('view_one_replay.html'
            , replay=replay
            , replay_details = replay_details
            )

def serve_replay(replay_id, filename):
    """Serve replays"""
    replay = Replay.query(Replay.filename == filename).get()
    #replay = Replay.get_by_id(int(replay_id))
    response = make_response(replay.replay_file)
    response.headers['Content-Type']='text/plain'
    return response

def list_replays(page=0
        , sortfield="timestamp"
        , sortorder="dsc"
        , replay_filter=""):
    replays, npages = process_listurl(page, sortfield, sortorder, replay_filter)
    for replay in replays:
        short_desc = replay['description'][:150]
        if len(short_desc) < len(replay['description']):
                short_desc += '...'
        replay['description'] = short_desc
    pagelist_html = get_pagelist_html(page, npages, sortfield, sortorder)
    return view_replay_list(replays, pagelist_html, replay_filter)

def view_replay_list(replays, pagelist_html, replay_filter):
    if replay_filter:
        replay_filter = '/' + replay_filter 
    return render_template('list_replays.html'
            , replays=prettify_scores(replays)
            , form=ReplayUploadForm()
            , replay_filter = replay_filter
            , pagelist_html = pagelist_html
            , title="2hu Replay Uploader")

def search():
    """Only displays replays with certain characteristics"""
    form= ReplaySearchForm()
    if request.method=="POST":
        if form.validate_on_submit():
            # All querying is processed in process_listurl
            replay_filter=build_replay_filter(form)
            return list_replays(0, 'timestamp', 'dsc', replay_filter)
        else:
          flash(u'One of your search inputs was invalid', 'Error')
          return redirect(url_for('search'))
    else:
        return render_template('search.html', form=form)

def upload_replay_with_user_input():
    form = ReplayUploadUserInputForm()
    if request.method == "GET":
        return redirect(url_for('upload_replay'))
    if request.method == "POST":
        game = request.form['game']
        form=generate_user_input_form(game)
        if form.validate_on_submit():
            replay = add_replay(form)
            if type(replay) in [str, unicode]:
                ''' 
                this means there was an error, which is generated in
                utilities.add_replay
                '''
                flash(replay, 'Error')
                return redirect(url_for('list_replays'))
            try:
                key = replay.put()
                # update timestamp
                round_timestamp_to_second(replay)
                replay_id = replay.key.id()
                flash(Markup(u'Replay %s successfully saved <a href="/replay_files/%s">here</a>.' % (replay.filename, replay_id)), 'success')
                return redirect(url_for('list_replays'))
            except CapabilityDisabledError:
                flash(u'App Engine Datastore is currently in read-only mode.', 'info')
                return redirect(url_for('list_replays'))
        else:
            flash(u'One of your upload inputs was invalid', 'Error')
            return redirect(url_for('list_replays'))

def upload_replay():
    if request.method == "GET":
        return render_template('new_replay.html', form=ReplayUploadForm(), game='')
    if request.method == "POST":
        form=ReplayUploadForm()
        if form.validate_on_submit():
            replay_file = form.replay_file.file.read()
            try:
                #game = get_game_from_fn(form.replay_file.file.filename)
                game = getRpyGame(replay_file)
                assert game in fn2ndb.values()
            except:
                flash(u'Please upload a valid replay. Accepted games are %s' % ', '.join(fn2ndb.values()), 'Error')
                return redirect(url_for('upload_replay_static'))
            replay = build_replay_from_file(replay_file, game)
            user_input_form = generate_and_fill_user_input_form(replay, game)
            return render_template('new_replay.html', form=user_input_form, game=game)
        else:
            flash(u'Your upload was invalid', 'Error')
            return redirect(url_for('upload_replay_static'))

def edit_replay(replay_id):
    replay = Replay.get_by_id(replay_id)
    if request.method == "POST":
        form = generate_edit_form(replay.game)
        if form.validate_on_submit():
            if form.password.data and secure_hash(form.password.data) == replay.password:
                replay.player = form.player.data
                replay.description = form.description.data
                replay.runtype = form.runtype.data
                replay.tas = form.tas.data
                replay.nm = form.nm.data
                replay.nb = form.nb.data
                replay.nf = form.nf.data
                replay.nv = form.nv.data
                replay.pacifist = form.pacifist.data
                replay.other_restriction = form.other_restriction.data
                replay.put()
                flash(u'Replay %s successfully updated.' % replay_id, 'success')
                return redirect(url_for('list_replays'))
            else:
                flash(u'Password for replay %s did not match.' % replay_id, 'error')
                return redirect(url_for('edit_replay', replay_id=replay_id))
    form = generate_and_fill_edit_form(replay,replay.game)
    return render_template('edit_replay.html', replay=replay, form=form, game=replay.game)

def delete_replay(replay_id):
    """Delete a replay object"""
    replay = Replay.get_by_id(replay_id)
    form = ReplayDeleteForm()
    if request.method == "POST":
        if form.validate_on_submit():
            if secure_hash(form.data.get('password')) == replay.password and replay.password:
                try:
                    replay.key.delete()
                    flash(u'Replay %s successfully deleted.' % replay_id, 'success')
                except CapabilityDisabledError:
                    flash(u'App Engine Datastore is currently in read-only mode.', 'info')
            else:
                flash(u'Password for replay %s did not match.' % replay_id, 'error')
        return redirect(url_for('list_replays'))
    return render_template('delete_replay.html', replay=replay, form=form)

def update_schema():
    ''' update the schema '''
    '''
    query = Replay.query()
    for replay in query:
        replay.game = 'DDC'
        replay.put()
    query = Counter.query()
    for counter in query:
        counter.game = 'DDC'
        counter.put()
    '''
    query = Replay.query()
    for replay in query:
        round_timestamp_to_second(replay)
        replay.put()
    return 'Updated timestamps!'

@admin_required
def admin_only():
    """This view requires an admin account"""
    return 'Super-seekrit admin page.'


@cache.cached(timeout=60)
def cached_examples():
    """This view should be cached for 60 sec"""
    examples = ExampleModel.query()
    return render_template('list_examples_cached.html', examples=examples)

def warmup():
    """App Engine warmup handler
    See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests

    """
    return ''

