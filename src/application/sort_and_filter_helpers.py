from models import Replay
from consts import *

diff2order = {
        '--':0
        , 'easy':1
        , 'normal':2
        , 'hard':3
        , 'lunatic':4
        , 'extra':5
        , 'phantasm':5
        }
game2order = {
        'eosd':6
        , 'pcb': 7
        , 'in': 8
        , 'pofv': 9
        , 'mof': 10
        , 'sa': 11
        , 'ufo': 12
        , 'td': 13
        , 'ddc':14
        , 'isc':14.3
        , 'lolk':15
        }

def page2query(page): 
    return ' LIMIT %d,%d' % (page*REPLAYS_PER_PAGE, REPLAYS_PER_PAGE)

def build_replay_filter(form):
    d = form.data
    url = ''
    for k in d:
        if d[k]:
            if type(d[k]) in (str, unicode):
                url += "%s=%s&" % (k, d[k])
            else: # if it's a boolean
                url += "%s=TRUE&" % (k)
    if url:
        if url[-1] == '&':
            url = url[:-1]
    return url 


def process_listurl(page, sortfield, sortorder, replay_filter):
    querystring = urlFilter2queryFilter(replay_filter)
    replays = Replay.gql(querystring).fetch()
    replays = [r.to_dict() for r in replays]

    player = check_urlfilter_for_player(replay_filter)
    if player:
        replays = [r for r in replays if player.lower() in r['player'].lower()]

    if sortfield == 'player':
        # defaults to asc order
        reverse_flag = (sortorder == 'dsc')
    else:
        #defaults to dsc order
        reverse_flag = not (sortorder == 'asc')
    if sortfield == 'score':
        replays.sort(key=lambda x: int(x[sortfield]), reverse=reverse_flag)
    elif sortfield == 'rank':
        replays.sort(key=lambda x: diff2order[x[sortfield].lower()], reverse=reverse_flag)
    elif sortfield == 'game':
        replays.sort(key=lambda x: game2order[x[sortfield].lower()], reverse=reverse_flag)
    elif sortfield == 'slow':
        replays.sort(key=lambda x: float(x[sortfield].split('%')[0]), reverse=reverse_flag)
    else:
        try:
            replays.sort(key=lambda x: x[sortfield].lower(), reverse=reverse_flag)
        except AttributeError:
            replays.sort(key=lambda x: x[sortfield], reverse=reverse_flag)
    replays = replays[page * REPLAYS_PER_PAGE: (page+1)*REPLAYS_PER_PAGE]

    npages = (len(replays)-1)/REPLAYS_PER_PAGE + 1
    return replays, npages

def urlFilter2queryFilter(replay_filter):
    replay_filter = replay_filter.replace("'",'')
    allowed_filterfields = [
            #'player'
            'runtype'
            , 'game'
            , 'char'
            , 'rank'
            , 'tas', 'nm', 'nb', 'nf', 'nv'
            , 'pacifist', 'other_restriction', 'cheese'
            ]
    if not replay_filter:
        return ''
    ret = ' WHERE '
    fs = replay_filter.split('&')
    for f in fs:
        field = f.split('=')[0]
        try:
            val = f.split('=')[1]
        except:
            continue
        if field not in allowed_filterfields:
            continue
        if field in ['runtype', 'game', 'char', 'rank']:
            val = "'%s'" % val
        ret += "%s = %s AND " % (field, val)
    if ret[-4:] == 'AND ':
        ret = ret[:-4]
    if ret == ' WHERE ':
        return ''
    else:
        return ret

def urlSort2querySort(field,order):
    allowed_sortfields=[
        'player'
        , 'playdate'
        , 'char'
        , 'rank'
        , 'stage'
        , 'score'
        , 'runtype'
        , 'timestamp'
        ]
    allowed_orders=['asc', 'dsc']
    o2o = {'asc':'ASC', 'dsc':'DESC'}
    ret = ' ORDER BY '
    if field not in allowed_sortfields:
        field = 'timestamp'
    ret += field + ' '
    if order not in allowed_orders:
        order = 'dsc'
    ret += o2o[order] + ' '
    return ret


def check_urlfilter_for_player(replay_filter):
    fs = replay_filter.split('&')
    for f in fs:
        s = f.split('=')
        if s[0] == 'player':
            return s[1]
    return ''

