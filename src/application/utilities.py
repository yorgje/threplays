from consts import *
import datetime as dt

def get_pagelist_html(page, npages, sortfield, sortorder):
    if npages:
        out = []
        if npages <= NPAGES_TO_LIST:
            for i in range(npages):
                out.append('<a href="/replays/%d/%s/%s">%d</a>' % (i, sortfield, sortorder, i))
        else:
            for i in range(NPAGES_TO_LIST/2):
                out.append('<a href="/replays/%d/%s/%s">%d</a>' % (i, sortfield, sortorder, i))
            for j in range(NPAGES_TO_LIST/2, NPAGES_TO_LIST):
                i = npages - ( j - NPAGES_TO_LIST/2 + 1)
                out.append('<a href="/replays/%d/%s/%s">%d</a>' % (i, sortfield, sortorder, i))
        if page < npages:
            out[page] = '<b>%s</b>' % out[page]
        return ', '.join(out)
    else:
        return '0'

def commas(n):
    return "{:,}".format(int(n))

def prettify_scores(replays):
    for replay in replays:
        replay['score'] = commas(replay['score'])
    return replays

def round_timestamp_to_second(replay):
    ts = replay.timestamp
    replay.timestamp = dt.datetime(ts.year, ts.month, ts.day, ts.hour, ts.minute, ts.second)
    replay.put()
